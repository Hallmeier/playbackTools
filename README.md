Small set of various tools, that aid in improving the music playback
experience in some way, I had programmed over some years and have now
bundled together in this R package.

# Installation

You can install this package from GitLab using `remotes`:

```r
remotes::install_gitlab("Hallmeier/playbackTools")
```

# Requirements

Some functions require [ffmpeg, ffplay and ffprobe](https://www.ffmpeg.org/) in 
your PATH, some work best together with the Windows version of 
[foobar2000](https://foobar2000.org/).

# Overview

`kisum()`  
: lets you play back random music backwards and guess the track

`stratifyPlaylist()`  
: is useful for shuffling different playlists together evenly

`adjustGain()` and `adjustOpusGain()`  
: help with batch-adjusting ReplayGain tags

`ratio2dB()`  
: converts power ratios to dB

# Documentation

After installation, access the documentation by `package?playbackTools`.
