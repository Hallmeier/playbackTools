# playbackTools 1.0.2 (2025-03-12)

* Upgrade to R 4.4. Since R natively handles UTF-8 on Windows since 4.2, all 
  UTF-8 enforcement code is removed.

# playbackTools 1.0.1 (2020-08-13)

* `kisum()`: Fix bug where duration was inferred incorrectly for some file 
  types.

# playbackTools 1.0.0 (2020-04-19)

Initial release with the exported functions

* `kisum()`
* `stratifyPlaylist()`
* `adjustGain()`
* `adjustOpusGain()`
* `ratio2dB()`
